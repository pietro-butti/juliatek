function read_tekconf(file::String,N::Int64,k::Int64,b::Float64,flag)
    pars = TEKconfPars(N,k,b,0,file)
    U = TEKconf(pars)

    if flag=="txt" # Masanori format
        catconf = readdlm(file)
        U.Pars.itraj = catconf[1,1]
        uread = catconf[2:N*N*4+1,:]

        global II = 1
        for mu in 1:4
            for j in 1:N
                for i in 1:N
                    U.link[mu][i,j] = uread[II,1] + uread[II,2]im
                    II+=1
                end
            end
        end
    end

    return U
end