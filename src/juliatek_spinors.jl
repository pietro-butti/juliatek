uno = one(ComplexF64)
O = zero(ComplexF64)
γ₁ = [
    O    O    O    -im;
    O    O   -im   O;
    O    im   O    O;
    im   O    O    O 
]
γ₂ = [
    O    O    O   -uno;
    O    O    uno  O;
    O    uno  O    O;
    -uno O    O    O 
]
γ₃ = [
    O    O   -im   O;
    O    O    O    im;
    im   O    O    O;
    O   -im   O    O 
]
γ₄ = [
    uno O   O    O   ;
    O   uno O    O   ;
    O   O   -uno O   ;
    O   O   O    -uno
]

struct Dirac
    gamma::Vector{Matrix{ComplexF64}}
    Dirac() = new([γ₁,γ₂,γ₃,γ₄])
end
function Base.show(io::IO,a::Dirac)
    print(io,"Dirac gamma matrices")
end

# """
#     This spinor have the structure 1₄ ⊗ ψ
# """
# mutable struct Spinor_adj{T}
#     Nc::Int64
#     color::Matrix{T}
#     Spinor_adj{T}(N::Int64) where T = new(N,randn(T,N,N)) 
#     Spinor_adj{T}(N::Int64,c::Matrix) where T = new(N,c)
# end
# function Base.show(io::IO,a::Spinor_adj)
#     print(io,"Adjoint fermion spinor (Nc=$(a.Nc))")
# end

# vector(ψ::Spinor_adj) = tensor(fill(1.,4),ψ.color)
# function spinor(V::Vector{T},Nc::Int64) where T 
#     length(V)%Nc^2 !=4 ? error("This vector is not a spinor like 1₄ ⊗ ψ") : 0

#     Vcheck = []
#     for i in 1:4
#         ii = collect(1:Nc^2) .+ (i-1)*Nc^2
#         append!(Vcheck,V[ii])
#     end
#     length(unique(Vcheck))==1 ? 0 : error("This vector is not a spinor")

#     ψcolor = reshape(V[1:Nc^2],(Nc,Nc))
#     return Spinor_adj{T}(ψcolor)
# end





#   QUESTA FUNZIONE PRENDE UN VETTORE A CAZZO Ψ IN INPUT E ASSUME CHE SIA 1₄ ⊗ ψ
function Dadj_psi(U::TEKconf,ψ::Vector{T},κ::Float64;r=1.) where T
    D = Dirac()

    ϕ = zeros(T,4*U.Pars.Nc^2)
    for μ in 1:length(U.link)
        pp = reshape(ψ[1:U.Pars.Nc^2],(U.Pars.Nc,U.Pars.Nc))

        dirac_p = (r*I - D.gamma[μ]) * [1,1,1,1]
        color_p = U.link[μ] * pp * U.link[μ]'

        dirac_m = (r*I + D.gamma[μ]) * [1,1,1,1]
        color_m = U.link[μ]' * pp * U.link[μ]
        
        ϕ -= (tensor(dirac_p,vec(color_p))+tensor(dirac_m,vec(color_m)))
    end
    ϕ = ψ - κ*ϕ

    return ϕ
end





 