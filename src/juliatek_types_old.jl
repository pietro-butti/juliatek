ϵ = [
    [ 0.,  1.,  1., 1.],
    [-1.,  0.,  1., 1.],
    [-1., -1.,  0., 1.],
    [-1., -1., -1., 0.]
]

"""
    twist(N,k)

This struct contains the twist factor with symmetric twist tensor.
"""
struct twist
    Nc::Int64
    kflux::Int64
    z::Matrix{ComplexF64}

    function twist(N,k)
        factor = 2*π*Float64(k)/sqrt(N)        
        z = Matrix{ComplexF64}(undef,4,4)
        [[
            z[i,j]=exp(factor*ϵ[i][j]*im) 

        for i in 1:4] for j in 1:4]

        return new(N,k,z)
    end
end

mutable struct TEKconf
    Nc::Int64
    kflux::Int64
    b::Float64
    itraj::Int64
    basement::String

    twist::twist
    link::Vector{Matrix{ComplexF64}}

    function TEKconf(N::Int64,k::Int64,b::Float64,II::Int64,name::String,links::Vector{Matrix{ComplexF64}}) 
        this = new(N,k,b,II,name)
        this.twist = twist(N,k)
        this.link = Vector{Matrix{ComplexF64}}(undef,4)
        for mu in 1:4
            this.link[mu] = links[mu]
        end
        return this
    end
    function TEKconf(N::Int64,k::Int64,b::Float64,II::Int64,name::String)
        this = new(N,k,b,II,name)
        this.twist = twist(N,k)
        this.link = Vector{Matrix{ComplexF64}}(undef,4)
        for mu in 1:4
            this.link[mu] = Matrix{ComplexF64}(undef,N,N)
        end
        return this
    end
    function TEKconf(_u::TEKconf)
        aux = deepcopy(_u)
        return new(aux.Nc,aux.kflux,aux.b,aux.itraj,aux.basement,aux.twist,aux.link)
    end
end



function Base.show(io::IO,a::TEKconf)
    print(io,"TEK configuration with (Nc=$(a.Nc), k=$(a.kflux), b=$(a.b)), read from $(a.basement)")
end


# function TEKconfInfo(io::IO,U::TEKconf,confstring::)
    # println(io,
        # ""
    # )
# end