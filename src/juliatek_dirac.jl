uno = one(ComplexF64)
O = zero(ComplexF64)
γ₁ = [
    O    O    O    -im;
    O    O   -im   O;
    O    im   O    O;
    im   O    O    O 
]
γ₂ = [
    O    O    O   -uno;
    O    O    uno  O;
    O    uno  O    O;
    -uno O    O    O 
]
γ₃ = [
    O    O   -im   O;
    O    O    O    im;
    im   O    O    O;
    O   -im   O    O 
]
γ₄ = [
    uno O   O    O   ;
    O   uno O    O   ;
    O   O   -uno O   ;
    O   O   O    -uno
]

struct Dirac
    gamma::Vector{Matrix{ComplexF64}}
    Dirac() = new([γ₁,γ₂,γ₃,γ₄])
end
function Base.show(io::IO,a::Dirac)
    print(io,"Dirac gamma matrices")
end




