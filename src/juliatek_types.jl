ϵ = [
    [ 0.,  1.,  1., 1.],
    [-1.,  0.,  1., 1.],
    [-1., -1.,  0., 1.],
    [-1., -1., -1., 0.]
]

"""
    twist(N,k)

This struct contains the twist factor with symmetric twist tensor.
"""
struct twist
    Nc::Int64
    kflux::Int64
    z::Matrix{ComplexF64}

    function twist(N,k)
        factor = 2*π*Float64(k)/sqrt(N)        
        z = Matrix{ComplexF64}(undef,4,4)
        [[
            z[i,j]=exp(factor*ϵ[i][j]*im) 

        for i in 1:4] for j in 1:4]

        return new(N,k,z)
    end
end


mutable struct TEKconfPars
    Nc::Int64
    kflux::Int64
    b::Float64
    itraj::Int64
    basement::String
end


mutable struct TEKconf
    Pars::TEKconfPars

    twist::twist
    link::Vector{Matrix{ComplexF64}}

    function TEKconf(p::TEKconfPars,links::Vector{Matrix{ComplexF64}}) 
        this = new(p)

        dim = length(links)

        this.twist = twist(p.Nc,p.kflux)
        this.link = Vector{Matrix{ComplexF64}}(undef,dim)
        [this.link[ii]=links[ii] for ii in 1:dim];

        return this
    end
    function TEKconf(p::TEKconfPars)
        this = new(p)

        this.twist = twist(p.Nc,p.kflux)
        this.link = Vector{Matrix{ComplexF64}}(undef,4)
        [this.link[ii]=Matrix{ComplexF64}(undef,p.Nc,p.Nc) for ii in 1:4]
    
        return this
    end
    function TEKconf(_u::TEKconf)
        aux = deepcopy(_u)
        return new(aux.Pars,aux.twist,aux.link)
    end
end


function Base.show(io::IO,a::TEKconf)
    print(io,"TEK configuration with (Nc=$(a.Pars.Nc), k=$(a.Pars.kflux), b=$(a.Pars.b)), read from $(a.Pars.basement)")
end


function LogInfo(a::TEKconf)
    p = a.Pars
    println(
    "# Configuration filename = $(p.basement) \n",
    "#          MC trajectory = $(p.itraj) \n",
    "#                     Nc = $(p.Nc) \n",
    "#                      k = $(p.kflux) \n",
    "#                      b = $(p.b)"
    )
end