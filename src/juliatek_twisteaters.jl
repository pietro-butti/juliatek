mutable struct thooft
    N::Int64
    kflux::Int64
    P::Matrix{ComplexF64}
    Q::Matrix{ComplexF64}
    
    function thooft(Nc::Int64,k::Int64)
        this = new(Nc,k)
    
        L = Int64(sqrt(Nc))
    
        # Shift 'tHooft matrix
        this.P = zeros(ComplexF64,L,L)
        [this.P[I]=0. for I in eachindex(this.P)]
        for i in 1:L-1
            this.P[i,i+1] = one(ComplexF64)
        end
        this.P[L,1] = one(ComplexF64)
    
        # # Clock 'tHooft matrix
        this.Q = zeros(ComplexF64,L,L)
        for i in 1:L
            this.Q[i,i] = exp( im*π*k * ((1-L)/L + 2*(i-1)/L) )
        end
    
        return this
    end
end


mutable struct TwistEaters
    N::Int64
    kflux::Int64
    mat::Vector{Matrix{ComplexF64}}

    function TwistEaters(Nc::Int64,k::Int64)
        this = new(Nc,k)
        t = thooft(Nc,k)
        one = Matrix{ComplexF64}(I,Int64(sqrt(Nc)),Int64(sqrt(Nc)))

        this.mat = Vector{Matrix{ComplexF64}}(undef,4)
        this.mat[1] = tensor(t.Q,t.Q)
        this.mat[2] = tensor(t.Q, t.P*t.Q)
        this.mat[3] = tensor(t.Q,t.P)
        this.mat[4] = tensor(t.P,one)

        return this
    end
end
