"""
    Path(v::Vector{Int64})

This structure encodes a path as a vector of directions. e.g. a plaquette in [1,3] plane
    plaquette = Path([1,3,-1,-3])
"""
struct Path
    links::Vector{Int64}
    is_closed::Bool
    Path(v::Vector{Int64}) = new(v,(sum(v)==0 ? true : false))
end

"""
    wilson_loop(U::TEKconf,C::Path)
    wilson_loop(U::Vector{Matrix{ComplexF64}},C::Path; verbose=true)

This function returns the product of gauge links following the `Path` `C`. 
WARNING: twist factor has to be inserted by hand.

"""
function wilson_loop(U::TEKconf,C::Path; verbose=false)
    if verbose
        C.is_closed ? 0 : println("WARNING: path is not closed. Returning something NOT gauge invariant")
    end

    W = I
    for μ in C.links
        W *= (μ>0 ? U.link[μ] : adjoint(U.link[-μ]))
    end

    return W
end
function wilson_loop(U::Vector{Matrix{ComplexF64}},C::Path; verbose=true)
    if verbose
        C.is_closed ? 0 : println("WARNING: path is not closed. Returning something NOT gauge invariant")
    end
    
    W = I
    for μ in C.links
        W *= (μ>0 ? U[μ] : adjoint(U[-μ]))
    end

    return W
end



"""
    plaquette(U::TEKconf)

This function returns the plaquette summed over μ!=ν normalized by Nc and the number of planes
"""
function plaquette(U::TEKconf)
    plaq = 0.
    planes = 0
    for ν in 1:length(U.link)
        for μ in 1:length(U.link)
            ν==μ ? continue : 0

            plaq += U.twist.z[μ,ν] * 
            # plaq += U.twist.z[ν,μ] * 
                tr(
                    wilson_loop( U.link, Path([μ,ν,-μ,-ν]) )
                )
            
            planes += 1.
        end
    end
    plaq /= (planes * U.Pars.Nc)

    return real(plaq)
end

"""
    action_wilson(U::TEKconf)

This returns S_w = 12*b*N²*(1-P)
"""
action_wilson(U::TEKconf) = (1. - plaquette(U)) * 12. * U.Pars.b * U.Pars.Nc^2 




"""
    function symmE(U::TEKconf)

Return symmetric definition of the plaquette according to
E = -1/128 ∑ Tr( (z_{mu,nu} *( UᵥUₘUᵥ'Uₘ' + UₘUᵥ'Uₘ'Uᵥ + Uᵥ'Uₘ'UᵥUₘ + Uₘ'UᵥUₘUᵥ ) - h.c.)² )
"""
function symmE(U::TEKconf)
    E = 0.
    for ν in 1:4
        for μ in 1:4
            ww = U.twist.z[ν,μ]*(
                wilson_loop(U,Path([ ν, μ,-ν,-μ])) +
                wilson_loop(U,Path([-ν,-μ, ν, μ])) + 
                wilson_loop(U,Path([ μ,-ν,-μ, ν])) + 
                wilson_loop(U,Path([-μ, ν, μ,-ν])) 
            )
            E += tr((ww - ww')^2)
        end
    end
    return real(E) / (-2. * 8. * 8. * U.Pars.Nc)
end


