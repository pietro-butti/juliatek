function Zforce(U::TEKconf)
    XX = Matrix{eltype(U.link)}(undef,4,4)
    for id in 1:4
        for jd in 1:4
            XX[id,jd] = zeros(ComplexF64,U.Pars.Nc,U.Pars.Nc)

            id==jd ? continue : 0

            XX[id,jd] = U.twist.z[id,jd]*(
                wilson_loop(U,Path([ id, jd,-id,-jd])) -
                wilson_loop(U,Path([-jd, id, jd,-id]))
            )

        end
    end

    H = Vector{eltype(U.link)}(undef,length(U.link))
    for id in 1:4
        H[id] = zeros(ComplexF64,U.Pars.Nc,U.Pars.Nc)
        for jd in 1:4
            id==jd ? continue : 0
            H[id] += .5*im* (XX[id,jd] - XX[id,jd]')
        end
    end

    return H
end

"""
    exphu(U::TEKconf,h::Vector{Matrix{ComplexF64}},Δt::Float64)

This function calculates exp(Δt*H)U and returns it as a vector of matrices
"""
function exphu(U::TEKconf, h::Vector{Matrix{ComplexF64}},Δt::Float64; taylor=false)
    newUlink = deepcopy(U.link)

    expHμ = zeros(ComplexF64,U.Pars.Nc,U.Pars.Nc)
    for μ in 1:4
        expHμ = exp(im*Δt*h[μ])
        expHμ = unitarize(expHμ,3)

        newUlink[μ] = expHμ*U.link[μ]
    end

    return newUlink
end

"""
    int_rk1(U::TEKconf,dt::Float64)

Runge-Kutta integrator of 1st order gauge for fields evolving under Wilson flow equation
    `U <= exp(i*Δt*Z[U])*U`
"""
function int_rk1(U::TEKconf,dt::Float64)
    H = Zforce(U)
    newlink = exphu(U,H,dt)

    newU = TEKconf(U.Pars,newlink)

    return newU
end


"""
    int_rk2(U::TEKconf,dt::Float64)

Runge-Kutta integrator of 3rd order for gauge fields evolving under Wilson flow equation

    Z₀ = Z[Uₜ]
    U₁ = exp{Z₀}*Uₜ

    Z₁ = Z[U₁]
    Uₜ₊ₑ = exp{Z₁/2 - Z₀/2}*U₁
"""
function int_rk2(U::TEKconf,dt::Float64)
    Z₀ = Zforce(U)
    U₁ = TEKconf(U.Pars, exphu(U,Z₀,dt) )

    Z₁ = Zforce(U₁)
    newU = TEKconf(U.Pars, exphu(U, Z₀./2 .- Z₁./2 ,dt) )

    return newU
end

"""
    int_rk3(U::TEKconf,dt::Float64)

Runge-Kutta integrator of 3rd order for gauge fields evolving under Wilson flow equation

    Z₀ = Z[Uₜ]
    U₁ = exp{Z₀/4}*Uₜ

    Z₁ = Z[U₁]
    U₂ = exp{8/9*Z₁ - 17/36*Z₀}*U₁

    Z₂ = Z[U₂]
    Uₜ₊ₑ = exp{3/4*Z₂ - 8/9*Z₁ + 17/36*Z₀}*U₂
"""
function int_rk3(U::TEKconf,dt::Float64)
    Z₀ = Zforce(U)
    U₁ = TEKconf(U.Pars, exphu(U,Z₀./4,dt))
    
    Z₁ = Zforce(U₁)
    U₂ = TEKconf(U.Pars, exphu(U₁, 8/9 .* Z₁ .- 17/36 .* Z₀ , dt) )
    
    Z₂ = Zforce(U₂)
    newU = TEKconf(U.Pars, exphu(U₂, 3/4 .* Z₂ .- 8/9 .* Z₁ .+ 17/36 .* Z₀ ,dt) )

    return newU
end



function PrintWilsonFlow(U::TEKconf,Δt::Float64,Nsteps::Int64; order=3)
    flowedU = deepcopy(U)

    println("# ========== Wilson Flow run $(now()) ============ ")
    println("# --------------- Configuration info -------------------  ")
    LogInfo(U)
    println("# --------------- Flow integration info  ---------------  ")
    println(
        "#                     ΔT = $Δt \n",
        "#                 Nsteps = $Nsteps \n",
        "#             int. order = $order"
        )
    println("# ------------------------------------------------------  ")
    @printf("%s",
        "# t             (1-P)              t²E(t) [plaq]      t²E(t) [clover]\n")

    # Initial measurements
    plq = plaquette(flowedU)
    @printf("%.7f       %.10f       %.10f       %.10f\n",0.,1-plq,0.,0.)

    tic = now()
    averageT = []
    for n in 1:Nsteps
        toc = now()

        T = n*Δt
        flowedU = TEKconf(int_rk3(flowedU,Δt))

        plq = plaquette(flowedU)
        clv = symmE(flowedU)

        @printf(
            "%.7f       %.10f       %.10f       %.10f\n",
            T, 1-plq, 12*T^2*(1-plq), T^2*clv 
        )

        dtime = round(now()-toc,Dates.Second)
        push!(averageT, dtime.value)
    end
    toc = now()


    println(
        "# --------------- Flow integration info  ---------------  ")

    println(
        "# ------------------Timing info ------------------------ \n ",
        "# Average int. step timing = $(round(mean(averageT),digits=2)) s \n",
        "#             Total timing = $(round(toc-tic,Dates.Second))"
    )


    return
end