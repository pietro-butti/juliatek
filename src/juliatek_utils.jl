function tensor(M::Matrix{T1},N::Matrix{T2}) where T1 where T2
    MoN = Matrix{Number}(undef,size(M,1)*size(N,1),size(M,2)*size(N,2))
    for I_M in 1:size(M,1)
        for J_M in 1:size(M,2)
            for i in 1:size(N,1)
                II = (I_M-1)*size(N,1) + i
                for j in 1:size(N,2)
                    JJ = (J_M-1)*size(N,2) + j
                    @inbounds MoN[II,JJ] = M[I_M,J_M]*N[i,j]
                end
            end

        end
    end

    return MoN
end
function tensor(v::Vector{T1},w::Vector{T2}) where T1 where T2
    vow = vcat([v_i .* w for v_i in v]...)
    return vow
end



"""
    unitarize(M::Matrix{ComplexF64},nstep::Int64; f1=.5,f2=3.)

This function projects M to a unitary matrix not too far from it.

Supposing that M is quasi-unitary, i.e. X=H*Ω where Ω ∈ SU(Nc), then
    `unitarize(X,nstep,0.5,3.0) = Ω` 
for Nc big enough
"""
function unitarize(M::Matrix{ComplexF64},nstep::Int64; f1=.5,f2=3.)
    X = deepcopy(M)
    Y = Matrix{ComplexF64}(undef,size(M,1),size(M,2))
    Z = Matrix{ComplexF64}(undef,size(M,1),size(M,2))

    for _ in 1:nstep
        Y = f1*f2*X
        Z = -f1*X*X'
        Y += Z*X
        X = Y
    end

    return X
end


