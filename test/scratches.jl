using juliatek, DelimitedFiles, LinearAlgebra, Printf

U = read_tekconf("test/n289b0360k5c010.link_0033",289,5,0.36,"txt")

## -----------------------------------------

# function mat(V::Vector,N::Int64)
#     return reshape(V,)
# end


mat(V::Vector) = reshape(V,Int64(sqrt(length(V))),Int64(sqrt(length(V))))






# # Create a random spinor
# Psi = randn(ComplexF64,U.Pars.Nc,U.Pars.Nc)

# function Dpsi_adj(ψ::Matrix{ComplexF64},U::TEKconf,κ::Float64)
#     dirac = Dirac()
#     unit = zeros(ComplexF64,4,4)
#     [unit[i,i]=one(ComplexF64) for i in 1:4];

#     ϕ = zeros(ComplexF64,16*U.Pars.Nc^2)
#     for μ in 1:4
#         one_m_gamma = I - dirac.gamma[μ]
#         one_p_gamma = I + dirac.gamma[μ]

#         UψUd = U.link[μ]*ψ*(U.link[μ]')
#         UdψU = (U.link[μ]')*ψ*U.link[μ]

#         ϕ -= vec(tensor(one_m_gamma,UψUd) + tensor(one_p_gamma,UdψU))
#     end
#     ϕ *= κ
#     ϕ += vec(tensor(unit,ψ))

#     return ϕ
# end





# ϕ = Dpsi_adj(Psi,U,0.1775)