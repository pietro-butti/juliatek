# # Display the number of physical and virtual cores on you machine
# using Hwloc
# Hwloc.num_physical_cores() 
# Hwloc.num_virtual_cores() 

# # When running multithreaded scripts, launch the script with the right variable activated:
# # JULIA_NUM_THREADS=<# threads> julia <scriptname>.jl

# # Say "Hello world from processor $id"
# @Threads.threads for i in 1:Threads.nthreads()
#     println("Hello, world! from processor $(Threads.threadid())")
# end

## --------------------------------------------------------------

using juliatek, LinearAlgebra, BenchmarkTools

U = read_tekconf("n289b0360k5c010.link_0033",289,5,0.36,"txt")

BLAS.set_num_threads(4)
PrintWilsonFlow(U,0.03125,10)




